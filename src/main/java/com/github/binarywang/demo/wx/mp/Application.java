package com.github.binarywang.demo.wx.mp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

import com.github.binarywang.demo.wx.mp.service.CustomMenuService;


/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@SpringBootApplication
public class Application implements CommandLineRunner, ApplicationListener<ApplicationReadyEvent> {

	public static final String appId = "wx330359ccb91ba359";
	
	@Autowired
	private CustomMenuService customMenuService;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		// TODO 初始化菜单
		customMenuService.createMenu(appId);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
