package com.github.binarywang.demo.wx.mp.service;

import com.github.binarywang.demo.wx.mp.config.WxMpConfiguration;

import me.chanjar.weixin.common.api.WxConsts.MenuButtonType;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.error.WxErrorException;

public class CustomMenuService {

	public void createMenu(String appId) {
		try {
			WxMenu menu = new WxMenu();
			WxMenuButton button1 = new WxMenuButton();
			button1.setType(MenuButtonType.CLICK);
			button1.setName("今日歌曲");
			button1.setKey("V1001_TODAY_MUSIC");

			WxMenuButton button2 = new WxMenuButton();
			button2.setType(MenuButtonType.VIEW);
			button2.setName("百度一下");
			button2.setUrl("http://www.baidu.com/");

			WxMenuButton button3 = new WxMenuButton();
			button3.setName("菜单");

			menu.getButtons().add(button1);
			menu.getButtons().add(button2);
			menu.getButtons().add(button3);

			WxMenuButton button31 = new WxMenuButton();
			button31.setType(MenuButtonType.VIEW);
			button31.setName("搜索");
			button31.setUrl("http://www.soso.com/");

			WxMenuButton button32 = new WxMenuButton();
			button32.setType(MenuButtonType.VIEW);
			button32.setName("视频");
			button32.setUrl("http://v.qq.com/");

			WxMenuButton button33 = new WxMenuButton();
			button33.setType(MenuButtonType.CLICK);
			button33.setName("赞一下我们");
			button33.setKey("V1001_GOOD");

			WxMenuButton button34 = new WxMenuButton();
			button34.setType(MenuButtonType.VIEW);
			button34.setName("获取用户信息");
			button34.setUrl("http://www.baidu.com/");

			button3.getSubButtons().add(button31);
			button3.getSubButtons().add(button32);
			button3.getSubButtons().add(button33);
			button3.getSubButtons().add(button34);

			WxMpConfiguration.getMpServices().get(appId).getMenuService().menuCreate(menu);
		} catch (WxErrorException e) {
			e.printStackTrace();
		}
	}
}
